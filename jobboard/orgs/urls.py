from django.urls import path, include
from . import views

#path('', views.IndexView.as_view(), name='index'),
#path('register/', views.UserFormView.as_view(), name='register'),

app_name = 'orgs'

urlpatterns = [

    path('', views.index, name='index'),

    path('login/', views.user_login, name="login"),

    path('prueba/', views.user_login1, name="index1"),

    path('logout/', views.logout, name='logout'),

    path('solicitud/<int:id_vacante>/', views.enviar_solicitud, name='solicitud'),

    path('organizacion/', views.organizacion, name='organizacion'),

    path('organizacion/informacion', views.informacionOrganizacion, name='organinfo'),

    path('organizacion/password/', views.passwordChange, name='orgspassword'),

    path('estudiante/', views.estudiante, name='estudiante'),

    path('organizacion/vacante_view' , views.vacante_view, name='vacanteview'),

    path('organizacion/vacante_details/<int:pk>' , views.vacante_details.as_view, name='vacantedetails'),

    path('organizacion/<int:id_vacante>/delete/', views.vacante_delete, name='vacantedelete'),

    path('estudiante/informacionvacante', views.vacante_edit, name='vacanteinfo'),

    path('estudiante/password/', views.passwordChange, name='orgspassword'),

    path('estudiante/<int:id_vacante>/', views.solicitud_delete, name='solicituddelete'),

    path('estudiante/informacion', views.informacionEstudiante, name='estudinfo'),

    path('estudiante/desactivar/', views.cuenta_desactivar, name='desactivar'),

    path('organizacion/desactivar/', views.cuenta_desactivar, name='desactivar'),
    path('i18n/', include('django.conf.urls.i18n')),
]
