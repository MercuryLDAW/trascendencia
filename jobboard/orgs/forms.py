from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm
from orgs.models import Organizacion, Estudiante, Vacante, Domicilio
from django.forms.models import inlineformset_factory
from django.utils.translation import ugettext_lazy as _

class RegistroOrganizacion(UserCreationForm):

    username = forms.CharField(label='Nombre de la organización', max_length=255, required=True, help_text="*", widget=forms.TextInput(attrs={'class': 'form-control'}))
    org_descripcion = forms.CharField(label='Descripción', max_length=255, required=False, help_text="")
    org_siglas = forms.CharField(label='Siglas', max_length=255, required=False, help_text="")
    org_figuralegal = forms.CharField(label='Figura legal', max_length=255, required=True, help_text="*")
    org_rfc = forms.CharField(label='RFC', max_length=255, required=True, help_text="*")
    org_cluni = forms.CharField(label='CLUNI', max_length=255, required=True, help_text="*")
    org_giro = forms.CharField(label='Giro/Área', max_length=255, required=True, help_text="*")
    org_webpg = forms.CharField(label='Página web ', max_length=255, required=False, help_text="")
    org_objetivo = forms.CharField(label="Objetivo", max_length=255, required=False, help_text="")
    org_campoaccion = forms.CharField(label="Campo acción", max_length=255, required=True, help_text="*")
    org_anios = forms.CharField(label='Años de operación', max_length=255, required=False)

    first_name = forms.CharField(label='Nombre de la persona a cargo de la organización', max_length=255, required=True, help_text="*", widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(label='Apellido de la persona a cargo de la organización', max_length=255, required=True, help_text="*", widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(label='Correo de la persona a cargo de la organización', max_length=255, required=True, help_text="*", widget=forms.TextInput(attrs={'class': 'form-control'}))
    org_persona_dpto = forms.CharField(label='Departamento de la persona a cargo de la organización', max_length=255, required=True, help_text="*")

    password1 = forms.CharField(label="Contraseña", widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(label="Confirmación de contraseña", widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Organizacion
        fields = (

            'username', 'org_descripcion', 'org_siglas', 'org_figuralegal',
            'org_rfc', 'org_giro', 'org_cluni', 'org_webpg', 'org_objetivo', 'org_campoaccion', 'org_anios', 'first_name', 'last_name',
            'email', 'org_persona_dpto', 'password1', 'password2')

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class PasswordChange(forms.Form):
    oldpassword = forms.CharField(label= _("Contraseña actual"), widget=forms.PasswordInput(attrs={'onChange': "password()", 'id': 'oldpass'}))
    newpassword1 = forms.CharField(label=_("Nueva contraseña"), widget=forms.PasswordInput(attrs={'onChange': "password()", 'id': 'pass1'}))
    newpassword2 = forms.CharField(label=_("Confirma nueva contraseña"), widget=forms.PasswordInput(attrs={'onChange': "password()", 'id': 'pass2'}))
#Si no jala con forms.Form, intentar con forms.ModelForm


class SignupEstudiante(UserCreationForm):
    CHOICES_ESCOLARIDAD = (
        ('SECUNDARIA', 'Secundaria'),
        ('PREPARATORIA', 'Preparatoria'),
        ('UNIVERSIDAD', 'Universidad')
    )

    CHOICES_ESTADO = (
        ('Aguascalientes', 'Aguascalientes'),
        ('Baja California', 'Baja California'),
        ('Baja California Sur', 'Baja California Sur'),
        ('Campeche', 'Campeche'),
        ('Chiapas', 'Chiapas'),
        ('Chihuahua', 'Chihuahua'),
        ('CDMX', 'CDMX'),
        ('Coahuila', 'Coahuila'),
        ('Colima', 'Colima'),
        ('Durango', 'Durango'),
        ('Estado de México', 'Estado de México'),
        ('Guanajuato', 'Guanajuato'),
        ('Guerrero', 'Guerrero'),
        ('Hidalgo', 'Hidalgo'),
        ('Jalisco', 'Jalisco'),
        ('Michoacan', 'Michoacan'),
        ('Morelos', 'Morelos'),
        ('Nayarit', 'Nayarit'),
        ('Nuevo León', 'Nuevo León'),
        ('Oaxaca', 'Oaxaca'),
        ('Puebla', 'Puebla'),
        ('Querétaro', 'Querétaro'),
        ('Quintana Roo', 'Quintana Roo'),
        ('San Luis Potosí', 'San Luis Potosí'),
        ('Sinaloa', 'Sinaloa'),
        ('Sonora', 'Sonora'),
        ('Tabasco', 'Tabasco'),
        ('Tamaulipas', 'Tamaulipas'),
        ('Tlaxcala', 'Tlaxcala'),
        ('Veracruz', 'Veracruz'),
        ('Yucatán', 'Yucatán'),
        ('Zacatecas', 'Zacatecas')
    )

    username = forms.CharField(label='Nombre de usuario', max_length=255, required=True, help_text="", widget=forms.TextInput(attrs={'class': 'form-control', 'id':'username_estudiante'}))
    est_fecha_nacimiento = forms.DateField(label='Fecha de nacimiento', required=True, help_text="MM/DD/AAAA", widget=forms.DateInput(attrs={'class': 'form-control'}))
    est_escolaridad = forms.CharField(label='Grado que estás cursando', required=True, widget=forms.Select(choices=CHOICES_ESCOLARIDAD, attrs={'class': 'form-control'}))
    est_ciudad = forms.CharField(label='Ciudad', max_length=255, required=False, help_text="", widget=forms.TextInput(attrs={'class': 'form-control'}))
    est_estado = forms.CharField(label='Estado', max_length=255, required=False, help_text="", widget=forms.Select(choices=CHOICES_ESTADO, attrs={'class': 'form-control'}))
    first_name = forms.CharField(label='Nombre(s)', max_length=255, required=True, help_text="*", widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(label='Apellido(s)', max_length=255, required=True, help_text="*", widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(label='Correo', max_length=255, required=True, help_text="*", widget=forms.TextInput(attrs={'class': 'form-control'}))
    password1 = forms.CharField(label="Contraseña", widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(label="Confirmación de contraseña", widget=forms.PasswordInput(attrs={'class': 'form-control'}))


    class Meta:
        model = Estudiante
        fields = (
            'username', 'est_fecha_nacimiento', 'est_escolaridad',
            'est_ciudad', 'est_estado', 'first_name', 'last_name', 'email', 'password1'
        )

class RegistroVacante(forms.ModelForm):
        CHOICES_TIPO_VACANTE = (
            ('S', _('Servicio social/voluntariado')),
            ('P', _('Práctica profesional'),)
        )
        CHOICES_HORARIO_VACANTE = (
            ('COMPLETO', _('Tiempo completo')),
            ('MEDIO', _('Medio tiempo')),
            ('FLEXIBLE', _('Horario flexible')),
        )
        CHOICES_AREAS_VACANTE = (
            ('ARQIOTECTURA', _('Arquitectura')),
            ('FINANZAS', _('Finanzas')),
            ('ARTE', _('Arte')),
            ('HUMANIDADES', _('Humanidades'))
        )
        CHOICES_APOYO_VACANTE = (
            ('TRANSPORTE', _('Transporte')),
            ('ALIMENTOS', _('Alimentos')),
            ('BECA', _('Beca')),
        )

        tipo = forms.CharField(label=_('Tipo de vacante'), max_length=255, required=True, help_text='', widget=forms.Select(choices=CHOICES_TIPO_VACANTE, attrs={'class': 'form-control'}))
        titulo = forms.CharField(label=_('Título'), max_length=255)
        #posiciones_disponibles = forms.PositiveSmallIntegerField(label='Posiciones disponibles', max_length=255)
        descripcion = forms.CharField(label=_('Descripción'), max_length=255)
        departamento = forms.CharField(label=_('Departamento relacionado'), max_length=255)
        duracion = forms.CharField(label=_('Duración'), max_length=255)
        horario = forms.CharField(label=_('Horario requerido'), max_length=255, required=True, help_text='', widget=forms.Select(choices=CHOICES_HORARIO_VACANTE, attrs={'class': 'form-control'}))
        areas = forms.CharField(label=_('Áreas relacionadas'), max_length=255, required=True, help_text='', widget=forms.Select(choices=CHOICES_AREAS_VACANTE, attrs={'class': 'form-control'}))
        apoyo = forms.CharField(label=_('Apoyo ofrecido'), max_length=255, required=True, help_text='', widget=forms.Select(choices=CHOICES_APOYO_VACANTE, attrs={'class': 'form-control'}))
        localizacion = forms.ModelChoiceField(Domicilio.objects, required=False, label=_('Domicilio registrado'))
        #nueva_localizacion = forms.CharField(label='Nuevo domicilio', required=False)


        class Meta:
            model = Vacante
            fields = ('tipo', 'titulo', 'descripcion', 'departamento', 'duracion', 'horario', 'areas', 'apoyo', 'localizacion')


class RegistroDomicilio(forms.ModelForm):
    exterior = forms.CharField(max_length=10)
    interior = forms.CharField(max_length=10)
    calle = forms.CharField(max_length=45)
    colonia = forms.CharField(max_length=45)
    cp = forms.CharField(max_length=5)
    longitud = forms.DecimalField(max_digits=30, decimal_places=15)
    latitud = forms.DecimalField(max_digits=30, decimal_places=15)

    class Meta:
        model = Domicilio
        fields = (
        'exterior', 'interior', 'calle', 'colonia', 'cp','longitud', 'latitud')
