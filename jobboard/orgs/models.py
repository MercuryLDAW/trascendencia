from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

class Organizacion(User):
    org_descripcion = models.CharField(max_length=255, verbose_name="Descripción")
    org_siglas = models.CharField(max_length=255, verbose_name="Siglas")
    org_figuralegal = models.CharField(max_length=255, verbose_name="Figura legal")
    org_rfc = models.CharField(max_length=255, verbose_name="RFC")
    org_cluni = models.CharField(max_length=255, verbose_name="CLUNI")
    org_webpg = models.CharField(max_length=255, verbose_name="Página web")
    org_objetivo = models.CharField(max_length=255, verbose_name="Objetivo")
    org_campoaccion = models.CharField(max_length=255, verbose_name="Campo acción")
    org_numvacantes = models.IntegerField(verbose_name="Número de vacantes totales", default=0)  # campo que no llena el usuario en el registro
    org_anios = models.CharField(max_length=255, verbose_name="Años de operación")
    org_persona_dpto = models.CharField(max_length=255, verbose_name="Departamento")

    class Meta:
        verbose_name="organización"
        verbose_name_plural="organizaciones"

class Estudiante(User):
    #username
    #email
    #password
    #first_name
    #last_name
    CHOICES_ESCOLARIDAD = (
        ('SECUNDARIA', 'Secundaria'),
        ('PREPARATORIA', 'Preparatoria'),
        ('UNIVERSIDAD', 'Universidad')
    )
    est_fecha_nacimiento = models.DateField(verbose_name="Fecha de nacimiento")
    est_escolaridad = models.CharField(max_length = 255, verbose_name="Escolaridad")
    est_ciudad = models.CharField(max_length=255, verbose_name="Ciudad")
    est_estado = models.CharField(max_length=255, verbose_name="Estado")
    est_horasAcreditadas = models.IntegerField(default='0', verbose_name="Horas acreditadas")
    class Meta:
        verbose_name="Estudiante"

class Domicilio(models.Model):
    exterior = models.CharField(max_length=10)
    interior = models.CharField(max_length=10)
    calle = models.CharField(max_length=45)
    colonia = models.CharField(max_length=45)
    cp = models.CharField(max_length=5)
    longitud = models.DecimalField(max_digits=30, decimal_places=15)
    latitud = models.DecimalField(max_digits=30, decimal_places=15)

    def __str__(self):
        return self.calle + ' ' + self.interior

class Vacante(models.Model):
    TIPO_VACANTE = (
        ('S', 'Servicio social/ voluntariado'),
        ('P', 'Práctica profesionales',)
    )
    HORARIO_VACANTE = (
        ('COMPLETO', 'Tiempo completo'),
        ('MEDIO', 'Medio tiempo'),
        ('FLEXIBLE', 'Horario flexible'),
    )
    AREAS_VACANTE = (
        ('ARQIOTECTURA', 'Arquitectura'),
        ('FINANZAS', 'Finanzas'),
        ('ARTE', 'Arte'),
        ('HUMANIDADES', 'Humanidades')
    )
    APOYO_VACANTE = (
        ('TRANSPORTE', 'Transporte'),
        ('ALIMENTOS', 'Alimentos'),
        ('BECA', 'Beca'),
    )

    tipo = models.CharField(choices=TIPO_VACANTE, max_length=255)
    titulo = models.CharField(max_length=255)
    #posiciones_disponibles = models.PositiveSmallIntegerField(max_length=255)
    descripcion = models.CharField(max_length=255)
    departamento = models.CharField(max_length=255)
    duracion = models.CharField(max_length=255)
    horario = models.CharField(choices=HORARIO_VACANTE, max_length=255)
    areas = models.CharField(choices= AREAS_VACANTE, max_length=255)
    apoyo = models.CharField(choices= APOYO_VACANTE, max_length=255)
    localizacion = models.ForeignKey(Domicilio, on_delete=models.CASCADE, null=True)
    org_vacante = models.ForeignKey(Organizacion, on_delete=models.CASCADE, null=False)

    def get_absolute_url(self):
        return reverse('orgs:organizacion',kwargs={'pk':self.pk})


    def __str__(self):
        return self.titulo + ' - ' + self.descripcion

class Moderador(User):
    class Meta:
        verbose_name="Moderador"
        verbose_name_plural="Moderadores"


 #TABLAS N-N

class vacante_estudiante(models.Model):
    vacante = models.ForeignKey(Vacante, on_delete=models.CASCADE, null=False)
    estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE, null=False)
    estado = models.CharField(max_length=20)

    def getVacante(self):
        return Vacante.objects.get(id=self.vacante.id)

    def getEstudiante(self):
        return Estudiante.objects.get(user_ptr_id=self.estudiante.id)


#evaluador_evaluado
class organizacion_estudiante(models.Model):
    evaluador = models.ForeignKey(Organizacion, on_delete=models.CASCADE, null=False)
    evaluado = models.ForeignKey(Estudiante, on_delete=models.CASCADE, null=False)
    empatia = models.IntegerField(verbose_name="Empatía")
    puntualidad = models.IntegerField(verbose_name="Puntualidad")
    companierismo = models.IntegerField(verbose_name="Compañerismo")
    responsabilidad = models.IntegerField(verbose_name="Responsabilidad")
    adaptacion = models.IntegerField(verbose_name="Adaptación al cambio")
    proactividad = models.IntegerField(verbose_name="Proactividad")
    actitud = models.IntegerField(verbose_name="Actitud")
