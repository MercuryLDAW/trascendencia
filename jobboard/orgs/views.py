from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout as django_logout
from django.contrib.auth.models import User, Group
from .forms import LoginForm, RegistroOrganizacion, SignupEstudiante, RegistroVacante,PasswordChange, RegistroDomicilio
from django.views.generic.detail import DetailView
from django.http import HttpResponse
from .models import Vacante, Organizacion, Estudiante, vacante_estudiante, organizacion_estudiante


def user_login1(request):
    return render(request, 'orgs/index1.html')


def index(request):

    try:
        orgs = Organizacion.objects.get(user_ptr_id=request.session['_auth_user_id'])
    except (KeyError, Organizacion.DoesNotExist):
        orgs = 0
    try:
        student = Estudiante.objects.get(user_ptr_id=request.session['_auth_user_id'])
    except (KeyError, Estudiante.DoesNotExist):
        student = 0

    vacante = Vacante.objects.all()
    if(request.method)=='POST':
        form_registro = RegistroOrganizacion(request.POST)

        if form_registro.is_valid():
            form_registro.save()
            cd = form_registro.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password1'])
            user_group = User.objects.get(id=user.id)
            group = Group.objects.get(name='Organizacion')
            group.user_set.add(user_group)
            #org_nombre = form.cleaned_data.get('org_nombre')
            username = form_registro.cleaned_data.get('username')
            password1 = form_registro.cleaned_data.get('password1')
            login(request, user)
            return redirect('orgs:organizacion')
        else:
            form_login = LoginForm(request.POST)

            if form_login.is_valid():
                cd = form_login.cleaned_data
                user = authenticate(username=cd['username'], password=cd['password'])
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        return render(request, 'orgs/organizacion.html')
                    else:
                        return HttpResponse('User is not active')
            else:
                form_registro_estudiante = SignupEstudiante(request.POST)
                print(form_registro_estudiante.is_valid())
                if form_registro_estudiante.is_valid():
                    form_registro_estudiante.save()

                    cd = form_registro_estudiante.cleaned_data
                    user = authenticate(username=cd['username'], password=cd['password1'])
                    user_group = User.objects.get(id=user.id)
                    group = Group.objects.get(name='Estudiante')
                    group.user_set.add(user_group)
                    login(request, user)
                    return redirect('orgs:estudiante')
    else:
        form_registro = RegistroOrganizacion()
        form_login = LoginForm()
        form_registro_estudiante = SignupEstudiante()
    return render(request, 'orgs/index.html', {'vacantes': vacante,'form_registro':form_registro, 'form_login':form_login, 'form_registro_estudiante':form_registro_estudiante, 'orgs': orgs, 'student': student})


def user_login(request):
    if request.method == 'POST':
        form_login = LoginForm(request.POST)
        if form_login.is_valid():
            cd = form_login.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                user_group = User.objects.get(id=user.id)
                if user.is_active:
                    login(request, user)
                    #org_group = Group.objects.filter(name='Organizacion')
                    if user_group.groups.filter(name='Organizacion').exists():
                        return redirect('orgs:organizacion')
                    elif user_group.groups.filter(name='Estudiante').exists():
                        return redirect('orgs:estudiante')
                else:
                    return HttpResponse('User is not active')
            else:
                return redirect('orgs:index')
        else:
            return HttpResponse('form is not valid')
    else:
        form_registro = RegistroOrganizacion()
        form_login = LoginForm()
        return render(request, 'orgs/sign_up.html', {'form_registro': form_registro, 'form': form_login})


def organizacion(request):
    orgs = Organizacion.objects.get(user_ptr_id=request.session['_auth_user_id'])
    vacantes = orgs.vacante_set.all()
    lista = Vacante.objects.all()
    form = RegistroVacante()
    formpass = PasswordChange()
    return render(request, 'orgs/organizacion.html',{'vacantes': vacantes, 'form': form, 'formpass': formpass, 'session': request.session['_auth_user_id'], 'orgs': orgs , 'lista': lista})


def informacionOrganizacion(request):
    organizacion = Organizacion.objects.get(user_ptr_id=request.session['_auth_user_id'])
    organizacion.org_descripcion = request.POST['descripcion']
    organizacion.org_siglas = request.POST['siglas']
    organizacion.org_figuralegal = request.POST['figuralegal']
    organizacion.org_rfc = request.POST['rfc']
    organizacion.org_cluni = request.POST['cluni']
    organizacion.org_webpg = request.POST['webpage']
    organizacion.org_objetivo = request.POST['objetivo']
    organizacion.org_campoaccion = request.POST['campodeaccion']
    organizacion.org_anios = request.POST['anios']
    organizacion.save()
    return redirect('orgs:organizacion')


def passwordChange(request):
    if request.method == 'POST':
        form = PasswordChange(request.POST)
        if form.is_valid():
            user = User.objects.get(id=request.session['_auth_user_id'])
            #user = get_object_or_404(User, id=request.session['_auth_user_id'])
            oldpass = form['oldpassword'].value()
            cd = form.cleaned_data
            user_test = authenticate(username=user.username, password=oldpass)
            newpass1 = form['newpassword1'].value()
            newpass2 = form['newpassword2'].value()
            if user_test is not None and newpass1 == newpass2:
                user.set_password(newpass1)
                user.save()
            user_group = User.objects.get(id=user.id)
            if user_group.groups.filter(name='Organizacion').exists():
                return redirect('orgs:organizacion')
            elif user_group.groups.filter(name='Estudiante').exists():
                return redirect('orgs:estudiante')
    else:
        #  PENDIENTE TERMINAR ESTA PARTE -----
        return redirect('orgs:organizacion')


def logout(request):
    django_logout(request)  #EL LOGOUT HACE EL FLUSH DE LA SESSION DE FORMA AUTOMATICA
    return redirect('orgs:index')


def estudiante(request):
    formpass = PasswordChange()
    user = Estudiante.objects.get(user_ptr_id=request.session['_auth_user_id'])
    vacantes_estudiante = user.vacante_estudiante_set.all()
    lista = Vacante.objects.all()
    return render(request,'orgs/estudiante.html', {'formpass': formpass, 'user': user, 'vacantes_estudiante': vacantes_estudiante, 'lista': lista})


def informacionEstudiante(request):
    estudiante = Estudiante.objects.get(user_ptr_id=request.session['_auth_user_id'])
    #estudiante.est_fecha_nacimiento = request.POST['fecha']    PENDIENTE LA PUTA FECHA
    estudiante.est_escolaridad = request.POST['escolaridad']
    estudiante.est_estado = request.POST['estado']
    estudiante.est_ciudad = request.POST['ciudad']
    estudiante.est_horasAcreditadas = request.POST['horas']
    estudiante.save()
    return redirect('orgs:estudiante')


def cuenta_desactivar(request):
    user = User.objects.get(id=request.session['_auth_user_id'])
    user.is_active = 0
    user.save()
    return redirect('orgs:index')


def vacante_view(request):
    if request.method == 'POST':
        form = RegistroVacante(request.POST)
        #domc = Domicilio(exterior=form.exterior, interior=form.interior, calle=form.calle, colonia=form.colonia, cp=form.cp, longitud=form.longitud, latitud=form.latitud)
        #domc = RegistroDomicilio(request.POST, request.FILES)
        if form.is_valid():
            #domc.save()
            vacante = form.save(commit=False)
            orgs = Organizacion.objects.get(user_ptr_id=request.session['_auth_user_id'])
            vacante.org_vacante = orgs
            vacante.save()
        return redirect('orgs:organizacion')
    else:
        #form = RegistroVacante()
        #formpass = PasswordChange()
        return redirect('orgs:organizacion')
        #return render(request, 'orgs/organizacion.html', {'form': form, 'formpass': formpass, 'session': request.session['_auth_user_id']})


class vacante_details(DetailView):
    model = Vacante
    template_name = 'details_vacante.html'


def vacante_edit(request):
    estudiante = Estudiante.objects.get(user_ptr_id=request.session['_auth_user_id'])
    estudiante.est_escolaridad = request.POST['escolaridad']
    estudiante.est_estado = request.POST['estado']
    estudiante.est_ciudad = request.POST['ciudad']
    estudiante.est_horasAcreditadas = request.POST['horas']
    estudiante.save()
    return redirect('orgs:organizacion')


def vacante_delete(request, id_vacante):
    Vacante.objects.get(id=id_vacante).delete()
    return redirect('orgs:organizacion')


def enviar_solicitud(request, id_vacante):
    estudiante = Estudiante.objects.get(user_ptr_id=request.session['_auth_user_id'])
    if not estudiante.vacante_estudiante_set.filter(vacante_id=id_vacante).exists():
        solicitud = vacante_estudiante(estado='pendiente', vacante_id=id_vacante, estudiante_id=request.session['_auth_user_id'])
        solicitud.save()
    return redirect('orgs:estudiante')


def solicitud_delete(request, id_vacante):
    vacante_estudiante.objects.get(id=id_vacante).delete()
    return redirect('orgs:estudiante')
