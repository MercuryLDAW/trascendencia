from django.contrib import admin
from django.contrib.auth.models import Permission, Group
from .models import Organizacion, Estudiante, Moderador



class OrgaAdmin(admin.ModelAdmin):
    list_display = ('username', 'org_siglas', 'org_cluni')
    list_filter = ['username']
    search_fields=['username', 'org_siglas']
    fieldsets = [
        ('Información general', {'fields':['username', 'org_siglas', 'org_cluni', 'org_rfc', 'date_joined', 'org_numvacantes']}),
        ('Detalles', {'fields':['org_figuralegal', 'org_descripcion', 'org_campoaccion']}),
        ('Información de la persona a cargo', {'fields':['first_name', 'last_name', 'org_persona_dpto']}),
        ]
    readonly_fields = ('username', 'org_siglas', 'org_cluni', 'org_rfc', 'date_joined',
                        'org_figuralegal', 'org_descripcion', 'org_campoaccion',
                        'first_name', 'last_name', 'org_persona_dpto')

class EstAdmin(admin.ModelAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name')
    list_filter = ['username', 'email', 'first_name', 'last_name']
    search_fields=['username', 'email']
    fieldsets = [
        ('Información general', {'fields':['username', 'email', 'first_name', 'last_name', 'est_horasAcreditadas']}),
        ('Información personal', {'fields':['est_fecha_nacimiento', 'est_ciudad', 'est_estado', 'est_escolaridad']}),
        ]
    readonly_fields = ('username', 'email', 'first_name', 'last_name', 'est_horasAcreditadas','est_fecha_nacimiento', 'est_ciudad', 'est_estado', 'est_escolaridad')

class UserStaff(admin.ModelAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name')
    list_filter = ['username', 'email', 'first_name', 'last_name']
    search_fields=['username', 'email']
    fieldsets = [
        (None, {'fields':['username', 'email', 'first_name', 'last_name', 'password']}),
        ('Info', {'fields':['last_login', 'date_joined']})
        ]
    readonly_fields = ('last_login', 'date_joined')
    def save_model(self, request, obj, form, change):
        if request.user.is_superuser:
            obj.set_password(raw_password = obj.password)
            obj.is_staff = True
            obj.save()
            user_group = Moderador.objects.get(id=obj.id)
            group = Group.objects.get(name='Organizacion')
            group.user_set.add(user_group)
            permission = Permission.objects.get(name='Can delete Estudiante')
            obj.user_permissions.add(permission)
            permission = Permission.objects.get(name='Can change Estudiante')
            obj.user_permissions.add(permission)
            permission = Permission.objects.get(name='Can delete Organizacion')
            obj.user_permissions.add(permission)
            permission = Permission.objects.get(name='Can change Organizacion')
            obj.user_permissions.add(permission)

admin.site.register(Moderador, UserStaff)
admin.site.register(Organizacion, OrgaAdmin)
admin.site.register(Estudiante, EstAdmin)
