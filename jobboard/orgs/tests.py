from django.test import TestCase
from .models import Estudiante, Organizacion, Moderador
from django.contrib.auth.models import User
from . import forms

class EstudiantesTestCase(TestCase):
    def setUp(self):
        m = Estudiante.objects.create(username="Miguel", est_fecha_nacimiento="1998-01-09")
        print(m.password)

    def test_estudiantes_exist(self):
        Miguel = Estudiante.objects.get(username="Miguel")
        self.assertEqual(Miguel.username, 'Miguel')
        self.assertNotEqual(Miguel.username, '')

    def test_sign_up(self):
        form_data = {'username' : 'miguelangelo',
                        'est_fecha_nacimiento' : '1998-01-09',
                        'est_escolaridad' : 'Secundaria',
                        'est_ciudad' : 'Irapuato',
                        'est_estado' : 'Guanajuato',
                        'first_name' : 'Miguel',
                        'last_name' : 'Navarro',
                        'email' : 'miguel.nvr98@gmail.com',
                        'password1' : 'contraseña',
                        'password2' : 'contraseña'}
        test_form = forms.SignupEstudiante(data=form_data)
        self.assertTrue(test_form.is_valid())
        test_form.save()
        Miguel = Estudiante.objects.get(username="miguelangelo")
        self.assertEqual(Miguel.username, 'miguelangelo')
        self.assertNotEqual(Miguel.username, '')
        form_data = {'username' : 'Miguel',
                'est_fecha_nacimiento' : '',
                'est_escolaridad' : '',
                'est_ciudad' : '',
                'est_estado' : '',
                'first_name' : '',
                'last_name' : '',
                'email' : '',
                'password1' : '',
                'password2' : ''}
        test_form = forms.SignupEstudiante(data=form_data)
        self.assertFalse(test_form.is_valid())

class OrganizacionesTestCase(TestCase):
    def setUp(self):
        Organizacion.objects.create(username="CRIT")
        Organizacion.objects.create()

    def test_organizaciones_exist(self):
        CRIT = Organizacion.objects.get(username="CRIT")
        self.assertEqual(CRIT.username, 'CRIT')
        self.assertNotEqual(CRIT.username, '')

    def test_organizacion_sign_up(self):
        form_data = {'username' : 'NiñosAfricanosDeJapon',
                        'org_descripcion' : 'ayudamos niños de japón',
                        'org_siglas' : 'NADJ',
                        'org_figuralegal' : '12345A',
                        'org_rfc' : 'LLLLNNLLLLLNNLL',
                        'org_cluni' : 'LLLLNNLLLLLNNLL',
                        'org_giro' : 'Ayuda a los discapacitados',
                        'org_webpg' : 'www.NADJ.io',
                        'org_objetivo' : 'Ayudar a los niños africanos de Japón',
                        'org_campoaccion' : 'Educación',
                        'org_anios' : '50',
                        'first_name' : 'Miguel',
                        'last_name' : 'Navarro',
                        'email' : 'miguel.nvr98@gmail.com',
                        'org_persona_dpto' : 'CEO',
                        'password1' : 'contraseña',
                        'password2' : 'contraseña'}
        test_form = forms.RegistroOrganizacion(data=form_data)
        self.assertTrue(test_form.is_valid())
        test_form.save()
        t1 = Organizacion.objects.get(username="NiñosAfricanosDeJapon")
        self.assertEqual(t1.username, 'NiñosAfricanosDeJapon')
        self.assertNotEqual(t1.username, '')
        form_data = {'username' : 'NiñosAfricanosDeJapon',
                        'org_descripcion' : '',
                        'org_siglas' : '',
                        'org_figuralegal' : '',
                        'org_rfc' : '',
                        'org_cluni' : '',
                        'org_giro' : '',
                        'org_webpg' : '',
                        'org_objetivo' : '',
                        'org_campoaccion' : '',
                        'org_anios' : '',
                        'first_name' : '',
                        'last_name' : '',
                        'email' : '',
                        'org_persona_dpto' : '',
                        'password1' : '',
                        'password2' : ''}
        test_form = forms.RegistroOrganizacion(data=form_data)
        self.assertFalse(test_form.is_valid())

class AdministratorTestCase(TestCase):
    def setUp(self):
        User.objects.create(username='admin', password='holo', is_superuser=True, is_staff=True, last_name='Navarro', first_name='Miguel')

    def test_administrator_deletes_organizacion(self):
        tAdmin = User.objects.get(username='admin')
        self.assertTrue(tAdmin.has_module_perms('can delete organización'))
        tOrga = Organizacion.objects.create(username="test")
        self.assertTrue(tOrga.delete())

    def test_administrator_deletes_estudiante(self):
        tAdmin = User.objects.get(username='admin')
        self.assertTrue(tAdmin.has_module_perms('can delete estudiante'))
        t1 = Estudiante.objects.create(username="test", est_fecha_nacimiento="1998-01-09")
        self.assertTrue(t1.delete())

    def test_administrator_creates_moderator(self):
        tAdmin = User.objects.get(username="admin")
        self.assertTrue(tAdmin.has_module_perms('can add moderador'))
        tModerador = Moderador.objects.create(username="moderador")
        self.assertTrue(Moderador.objects.get(username="moderador"))

    def test_administrator_deletes_moderator(self):
        tAdmin = User.objects.get(username='admin')
        self.assertTrue(tAdmin.has_module_perms('can delete moderador'))
        t1 = Moderador.objects.create(username="test")
        self.assertTrue(t1.delete())

    def test_administrator_changes_password(self):
        tAdmin = User.objects.get(username="admin")
        tAdmin.password = "hola"
        tAdmin.save()
        self.assertEqual(tAdmin.password, "hola")

    def test_administrator_changes_moderators_password(self):
        tAdmin = User.objects.get(username="admin")
        self.assertTrue(tAdmin.has_module_perms('can change moderador'))
        t1 = Moderador.objects.create(username="test", password="hola")
        t1.password = "holo"
        t1.save()
        self.assertEqual(tAdmin.password, "holo")

    def test_administrator_changes_organizacion_numvacantes(self):
        tAdmin = User.objects.get(username='admin')
        self.assertTrue(tAdmin.has_module_perms('can change organización'))
        tOrga = Organizacion.objects.create(username="test", org_numvacantes=4)
        tOrga.org_numvacantes = 2
        tOrga.save()
        self.assertEqual(tOrga.org_numvacantes, 2)

    def test_administrator_modifies_personal_information(self):
        tAdmin = User.objects.get(username="admin")
        tAdmin.first_name = "miguelito"
        tAdmin.save()
        self.assertEqual(tAdmin.first_name, "miguelito")
# Create your tests here.
