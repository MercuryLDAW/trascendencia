from django.db import models

class Organizacion(models.Model):
    org_nombre = models.CharField(max_length=255)
    org_siglas = models.CharField(max_length=255)
    org_figuralegal = models.CharField(max_length=255)
    org_rfc = models.CharField(max_length=255)
    org_cluni = models.CharField(max_length=255)
    org_webpg = models.CharField(max_length=255)
    org_objetivo = models.CharField(max_length=255)
    org_campoaccion = models.CharField(max_length=255)
    org_persona_nombre = models.CharField(max_length=255)
    org_persona_correo = models.CharField(max_length=255)
    org_persona_dpto = models.CharField(max_length=255)
    org_numvacantes = models.CharField(max_length=255)

# Create your models here.
